<?php

namespace Ds\Database;

use Ds\Authenticate\TokenInterface;
use Ds\EasyCache\CacheInterface;

/**
 * Database
 *
 * @package Ds\Database
 * @author  Dan Smith <dan--smith@hotmail.co.uk>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */
class Database implements DatabaseInterface
{

    /**
     * @var ConnectionInterface
     */
    public $connection;

    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * @var TokenInterface
     */
    protected $token;

    /**
     * Database constructor.
     *
     * @param ConnectionInterface $connection
     * @param CacheInterface      $cache
     * @param TokenInterface      $token
     */
    public function __construct(
        ConnectionInterface $connection,
        CacheInterface $cache,
        TokenInterface $token
    ) {
        $this->connection = $connection;
        $this->cache = $cache;
        $this->token = $token;
    }

    /**
     * @inheritdoc
     */
    public function getCacheInterface()
    {
        return $this->cache;
    }

    /**
     * @inheritdoc
     */
    public function withCacheInterface(CacheInterface $cache)
    {
        $new = clone $this;
        $new->cache = $cache;
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function withConnection(ConnectionInterface $connection)
    {
        $new = clone $this;
        $new->connection = $connection;
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @inheritdoc
     */
    public function update($statement, array $args = [], array $options = [])
    {
        $throw = isset($options[strtolower('throw')]) ?: false;

        try {
            $this->connection->update($statement, $args);
            return true;
        } catch (DatabaseException $e) {
            if ($throw) {
                throw new \Exception($e->getMessage());
            }
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function fetch($statement, array $args = [], array $options = [])
    {
        $throw = isset($options[strtolower('throw')]) ?: false;
        $expires = isset($options[strtolower('expires')]) ?: 0;
        $cacheKey = isset($options[strtolower('key')]) ?: 'fetchSql';

        $queryKey = $this->token->createFromString($statement.json_encode($args), $cacheKey);
        $cache = $this->cache->getCacheStorage();

        if ($cache){
            $res = $cache->get($queryKey);
            if (null !== $res){
                return $res;
            }
        }
        
        try {
            $data = $this->connection->fetch($statement, $args);
           
            if ($cache){
                $cache->set($queryKey, $data, $expires);
            }
            return $data;
        } catch (DatabaseException $e) {
            if ($throw) {
                throw new \Exception($e->getMessage());
            }
            return false;
        }
    }
}
