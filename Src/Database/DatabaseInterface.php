<?php

namespace Ds\Database;

use Ds\EasyCache\CacheInterface;

/**
 * Interface DatabaseInterface
 *
 * @package Ds\Database
 * @author  Dan Smith <dan--smith@hotmail.co.uk>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */
interface DatabaseInterface extends ConnectionInterface
{

    /**
     * @param CacheInterface $cache
     * @return DatabaseInterface
     */
    public function withCacheInterface(CacheInterface $cache);

    /**
     * @return CacheInterface
     */
    public function getCacheInterface();

    /**
     * Returns Database with added ConnectionInterface
     *
     * @param  ConnectionInterface $connection
     * @return DatabaseInterface
     */
    public function withConnection(ConnectionInterface $connection);

    /**
     * Get ConnectionInterface
     *
     * @return ConnectionInterface
     */
    public function getConnection();
}
