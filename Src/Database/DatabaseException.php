<?php

namespace Ds\Database;

/**
 * Class DatabaseException
 *
 * @package Ds\Database
 * @author  Dan Smith <dan--smith@hotmail.co.uk>
 * @license MIT
 * @license https://opensource.org/licenses/MIT
 */
class DatabaseException extends \Exception
{
}
