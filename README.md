# Database
Database wrapper with PDO adaptor.

```
$database = new \Ds\Database\Database(
    new \Ds\Database\Connections\PDOConnection('localhost','database','user','password'),
    new \Ds\Cache\Cache(),
    new \Ds\Authenticate\Token()
);

```

```
//Query Database
$data = $database->fetch('SELECT `column` FROM table',[]);

//Loop through results
foreach ($data as $result){
    echo $result->column;
}

//Cache Results using memcache
$cache =new \Ds\Cache\Cache(
    new \Ds\Cache\MemcacheStorage(
        new \Memcached()
    )
);

$database = $database->withCacheInterface($cache)
$data = $database->fetch('SELECT `column_a` FROM table',[], 360);

//if requested within 360 seconds fetched from cache instead of calling database
$data = $database->fetch('SELECT `column_a` FROM table',[], 360);

```