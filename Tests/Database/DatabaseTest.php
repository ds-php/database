<?php

namespace Tests\Database;

use Ds\Database\Database;
use Ds\Database\ConnectionInterface;
use Ds\EasyCache\CacheInterface;
use Ds\EasyCache\CacheStorageInterface;
use Ds\Authenticate\TokenInterface;

/**
 * Class DatabaseTest
 * @package Tests\Database
 */
class DatabaseTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Database
     */
    public $database;

    /**
     * @var ConnectionInterface
     */
    public $connection;

    /**
     * @var CacheStorageInterface
     */
    protected $storage;

    /**
     * @var CacheInterface
     */
    public $cache;
    /**
     * @var TokenInterface
     */
    public $token;

    /**
     *
     */
    public function setUp() : void
    {
        $this->connection = $this->getMockBuilder(ConnectionInterface::class)->getMock();
        $this->storage = $this->getMockBuilder(CacheStorageInterface::class)->getMock();
        $this->cache = $this->getMockBuilder(CacheInterface::class)->getMock();
        $this->token = $this->getMockBuilder(TokenInterface::class)->getMock();

        $this->database = new Database($this->connection, $this->cache, $this->token);
    }

    /**
     *
     */
    public function testWithConnection()
    {
        $newConnection = $this->getMockBuilder(ConnectionInterface::class)
            ->getMock();

        $newDatabase = $this->database->withConnection($newConnection);
        $this->assertEquals($newConnection, $newDatabase->getConnection());
    }

    /**
     *
     */
    public function testGetConnection()
    {
        $connection = $this->database->getConnection();
        $this->assertEquals($this->connection, $connection);
    }

    /**
     *
     */
    public function testWithCacheInterface()
    {
        $cacheInterface = clone $this->cache;
        $newDatabase = $this->database->withCacheInterface($cacheInterface);
        $this->assertNotSame($this->cache, $newDatabase->getCacheInterface());
        $this->assertInstanceOf(CacheInterface::class, $newDatabase->getCacheInterface());
    }

    /**
     *
     */
    public function testGetCacheInterface()
    {
        $expected = CacheInterface::class;
        $actual = $this->database->getCacheInterface();
        $this->assertSame($this->cache, $actual);
        $this->assertInstanceOf($expected, $actual);
    }

    /**
     *
     */
    public function testUpdate()
    {
        $sql = 'update name :bar where id = bar';
        $args = ['bar' => 'baz'];

        $this->connection->expects($this->once())
            ->method('update')
            ->with(
                $this->equalTo($sql),
                $this->equalTo($args)
            );

        $this->database->update($sql, $args);
    }


    /**
     *
     */
    public function testFetch()
    {
        $sql = 'select * from foo where :bar = bar';
        $args = [':bar' => 'baz'];

        $this->token->expects($this->once())
            ->method('createFromString')
            ->willReturn('jsonKey');

        $this->cache->expects($this->once())
        ->method('getCacheStorage')
        ->willReturn(false);

        $this->connection->expects($this->once())
            ->method('fetch')
            ->with(
                $this->equalTo($sql),
                $this->equalTo($args)
            );

        $this->database->fetch($sql, $args);
    }

    /**
     *
     */
    public function testFetchCached()
    {
        $sql = 'select * from foo where :bar = bar';
        $args = [':bar' => 'baz'];

        $this->token->expects($this->once())
            ->method('createFromString')
            ->willReturn('jsonKey');

        $this->cache->expects($this->once())
            ->method('getCacheStorage')
            ->willReturn($this->storage);

        $this->storage->expects($this->once())
            ->method('get')
            ->willReturn('extractedCacheValue');

        $expected = 'extractedCacheValue';
        $actual = $this->database->fetch($sql, $args, ['cachekey' => 'jsonKey']);

        $this->assertEquals($expected, $actual);
    }
}
