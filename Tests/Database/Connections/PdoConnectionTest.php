<?php

namespace Tests\Database\Connections;

use Ds\Database\Connections\PDOConnection;
use Ds\Database\DatabaseException;
use \PDO;

/**
 * Class PDOExceptionClass
 * @package Tests\Database\Connections
 */
class PDOExceptionClass extends \PDO
{
    /**
     * PDOExceptionClass constructor.
     */
    public function __construct(){}

    /**
     * @param string $statement
     * @param null $options
     */
    public function prepare($statement, $options = null)
    {
        throw new \PDOException('PDOException');
    }

    /**
     *
     */
    public function execute()
    {
        return;
    }

    /**
     * @throws \Exception
     */
    public function fetchAll()
    {
        throw new \Exception('PDOException');
    }
}

/**
 * Class PdoConnectionTest
 * @package Tests\Database\Connections
 */
class PdoConnectionTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var PDOConnection
     */
    public $pdo;
    /**
     * @var \PDO
     */
    public $pdoMock;

    /**
     *
     */
    public function setUp() : void
    {
        $this->pdo = new PDOConnection(null);
        $this->pdoMock = $this->getMockBuilder(\PDO::class)->disableOriginalConstructor()->getMock();

    }

    /**
     *
     */
    public function testNoDB()
    {
        $this->expectException(DatabaseException::class);
        $this->pdo = new PDOConnection("mysql:host=localhost;dbname=missing", 'root', 'password');
        $this->pdoMock = $this->getMockBuilder(PDOConnection::class)->disableOriginalConstructor()->getMock();
    }

    /**
     *
     */
    public function testSetDefaultOptions()
    {
        $actual = $this->pdo->setOptions();
        $expected = [
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_OBJ,
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testSetCustomOptions()
    {
        $options = [
            \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        ];
        $actual = $this->pdo->setOptions($options);
        $expected = $options;
        $this->assertEquals($expected, $actual);
    }

    /**
     * todo: add reflection class, replace call to get
     */
    public function testWithConnection()
    {
        $newPdo = $this->pdo->withConnection($this->pdoMock);
        $expected = PDO::class;
        $actual = $newPdo->getConnection();
        $this->assertInstanceOf($expected, $actual);
    }

    /**
     *
     */
    public function testGetConnection()
    {
        $newPdo = $this->pdo->withConnection($this->pdoMock);
        $expected = PDO::class;
        $actual = $newPdo->getConnection();
        $this->assertInstanceOf($expected, $actual);
    }

    /**
     *
     */
    public function testGetLastId()
    {
        $expected = null;
        $actual = $this->pdo->getLastId();
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testFetch()
    {


        $query = 'SELCT * from Table';

        $newPdo = $this->pdo->withConnection($this->pdoMock);

        $this->pdoMock->expects($this->once())
            ->method('prepare')
            ->with(
                $this->equalTo($query)
            );

        $newPdo->fetch($query, []);
    }

    /**
     *
     */
    public function testFetchException()
    {

        $query = 'SELCT * from Table';
        $this->expectException('Exception');

        $newPdo = $this->pdo->withConnection($this->pdoMock);

        $exception = new PDOExceptionClass();

        $this->pdoMock->expects($this->once())
            ->method('prepare')
            ->willReturn($exception);

        $newPdo->fetch($query, []);
    }

    /**
     *
     */
    public function testUpdate()
    {
        $query = 'UP';

        $newPdo = $this->pdo->withConnection($this->pdoMock);

        $this->pdoMock->expects($this->once())
            ->method('prepare')
            ->with(
                $this->equalTo($query)
            );

        $newPdo->update($query, []);
    }


    /**
     * Begin Transaction.
     */
    public function testBeginTransaction()
    {

        $newPdo = $this->pdo->withConnection($this->pdoMock);

        $this->pdoMock->expects($this->once())
            ->method('beginTransaction');

        $newPdo->beginTransaction();
    }

    /**
     * Rollback Transaction.
     */
    public function testRollBack()
    {
        $newPdo = $this->pdo->withConnection($this->pdoMock);

        $this->pdoMock->expects($this->once())
            ->method('rollBack');

        $newPdo->rollBack();
    }

    /**
     * Commit Transaction.
     */
    public function testCommit()
    {
        $newPdo = $this->pdo->withConnection($this->pdoMock);

        $this->pdoMock->expects($this->once())
            ->method('commit');

        $newPdo->commit();
    }
}
