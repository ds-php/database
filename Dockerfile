FROM php:7.4-cli
COPY . /usr/src/myapp
WORKDIR /usr/src/myapp

RUN pecl install redis-5.1.1 && pecl install xdebug-2.8.1 \
  && docker-php-ext-enable redis xdebug \
  && apt-get update \
  && apt-get install libzip-dev -y \
  && docker-php-ext-configure zip && docker-php-ext-install zip \
  && docker-php-ext-configure pdo_mysql && docker-php-ext-install pdo_mysql \
  && docker-php-ext-configure pdo && docker-php-ext-install pdo \
  && php -m

RUN ./vendor/bin/phpunit --configuration phpunit.xml --coverage-text
